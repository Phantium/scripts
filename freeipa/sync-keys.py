#!/usr/bin/env python

import ldap
import os

# Credentials for the old LDAP that you're importing keys from
username = "uid=USERNAME,ou=Users,dc=YOURDOMAIN,dc=EXT"
password = "PASSWORD"

# Keys that we'll import, modify to your liking
ssh_key_types_allowed = ["ssh-rsa", "ssh-ed25519"]
# sshPublicKey is quite common, you can change it if necessary
pubkey_property = "sshPublicKey"

c = ldap.initialize("ldap://yourdomain.ext:389")
c.protocol_version = ldap.VERSION3
c.simple_bind(username, password)

dn = "ou=users,dc=YOURDOMAIN,dc=EXT"

result = c.search_s(dn, ldap.SCOPE_SUBTREE, "(objectClass=person)", ["uid", pubkey_property])
for uid, sshPublicKey in result:
    if not sshPublicKey.get(pubkey_property):
        # do nothing
        print("user {} has no public keys, skipping.".format(sshPublicKey.get('uid')[0]))
    elif len(sshPublicKey.get(pubkey_property)) == 1:
        print("user {} has 1 public key".format(sshPublicKey.get('uid')[0], sshPublicKey.get(pubkey_property)[0]))

        # clear old keys
        os.system("/usr/bin/ipa user-mod {} --sshpubkey=''".format(format(sshPublicKey.get('uid')[0])))

        # ipa user-mod USER --sshpubkey=
        # add public key to freeipa
        if sshPublicKey.get(pubkey_property)[0].split(" ", 1)[0] in ssh_key_types_allowed:
            os.system("/usr/bin/ipa user-mod {} --sshpubkey='{}'".format(format(sshPublicKey.get('uid')[0]), sshPublicKey.get(pubkey_property)[0]))
        print("")
    elif len(sshPublicKey.get(pubkey_property)) > 1:
        print("user {} has more than 1 public key.".format(sshPublicKey.get('uid')[0]))

        userkeys = ""
        for key in sshPublicKey.get(pubkey_property):
            # create string list with --sshpubkey='KEY' for each of the ssh keys

            # Limit keys to import
            if key.split(" ", 1)[0] in ssh_key_types_allowed:
                userkeys +=str(" --sshpubkey='{}'".format(key))

            # clear old keys
            os.system("/usr/bin/ipa user-mod {} --sshpubkey=''".format(format(sshPublicKey.get('uid')[0])))

            # Add keys to user with more than 1 key
            os.system("/usr/bin/ipa user-mod {} {}".format(format(sshPublicKey.get('uid')[0]), userkeys))

        print("")
