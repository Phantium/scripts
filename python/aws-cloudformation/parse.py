import json
try:
    import click
except ImportError:
    import click
    print("Missing click package: pip install click")
    exit(1)

# Default values
instance_name = 'MyNewInstance'


@click.command()
@click.option('--instances', default=1, help='Number of instances to create')
@click.option('--instance-type', default='t2.micro', help='Instance type')
@click.option('--allow-ssh-from', default='0.0.0.0/0', help='IP address to allow SSH for')
@click.option('--image-id', default='ami-b97a12ce', help='Image to use')
def generate_json(instances, instance_type, allow_ssh_from, image_id):
    # Generate AWS Cloudformation template
    if not allow_ssh_from == '0.0.0.0/0':
        allow_ssh_from = allow_ssh_from + '/32'

    json_dict = {}
    json_dict['AWSTemplateFormatVersion'] = '2010-09-09'
    json_dict['Outputs'] = {}
    json_dict['Outputs']['PublicIP'] = {}
    json_dict['Outputs']['PublicIP']['Description'] = 'Public IP address of the newly created EC2 instance'
    json_dict['Outputs']['PublicIP']['Value'] = {}
    json_dict['Outputs']['PublicIP']['Value']['Fn::GetAtt'] = ['EC2Instance', 'PublicIp']
    json_dict['Resources'] = {}

    # Loop over instances and create objects
    for i in range(0, instances):
        instancename = "{}{}".format(instance_name, i)  # Sets naming scheme to use

        json_dict['Resources'][instancename] = {}
        json_dict['Resources'][instancename]['Properties'] = {}
        json_dict['Resources'][instancename]['Properties']['ImageId'] = image_id
        json_dict['Resources'][instancename]['Properties']['InstanceType'] = instance_type
        json_dict['Resources'][instancename]['Properties']['SecurityGroups'] = list()
        json_dict['Resources'][instancename]['Properties']['SecurityGroups'].append(dict(Ref='InstanceSecurityGroup'))
        json_dict['Resources'][instancename]['Type'] = 'AWS::EC2::Instance'  # Object type is an instance

    # Generate firewall policy
    json_dict['Resources']['InstanceSecurityGroup'] = {}
    json_dict['Resources']['InstanceSecurityGroup']['Properties'] = {}
    json_dict['Resources']['InstanceSecurityGroup']['Properties']['GroupDescription'] = 'Enable SSH access via port 22'
    json_dict['Resources']['InstanceSecurityGroup']['Properties']['SecurityGroupIngress'] = list()
    json_dict['Resources']['InstanceSecurityGroup']['Properties']['SecurityGroupIngress'].append(dict(CidrIp=allow_ssh_from, FromPort='22', IpProtocol='tcp', ToPort='22'))
    json_dict['Resources']['InstanceSecurityGroup']['Type'] = 'AWS::EC2::SecurityGroup'

    click.echo(json.dumps(json_dict, sort_keys=True, indent=4))

if __name__ == '__main__':
    generate_json()
