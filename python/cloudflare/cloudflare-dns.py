#!/usr/bin/env python

# Author:
#   Dominique Debyttere
# Purpose:
#   Simple dynamic DNS script

import aiohttp
import asyncio
import CloudFlare

ip_url_address = 'http://ip.raw.re'  # Website providing IP lookup, can be anything as long it outputs just the IP.
username = 'yourmail@yourdomain.com'  # Username for CloudFlare
apikey = 'yourapikey'  # API key for CloudFlare
hostname = 'your.hostname.com'  # Subdomain to use for dynamic DNS
domainname = hostname.split('.', 1)[1]

loop = asyncio.get_event_loop()
cf = CloudFlare.CloudFlare(email=username, token=apikey)

async def getip():
    with aiohttp.ClientSession() as session:
        async with session.get(ip_url_address) as response:
            return await response.text()


def checkip():
    # Zone
    zone_info = cf.zones.get(params={'name': domainname})

    # Safety checks
    if len(zone_info) == 0:
        exit('Zone {} not found'.format(domainname))
    if len(zone_info) != 1:
        exit('Got {} zones, expected exactly 1'.format(len(zone_info)))

    zone_id = zone_info[0]['id']

    # Record
    dns_record = cf.zones.dns_records.get(zone_id, params={'name': hostname})

    if len(dns_record) == 0:
        exit('Record {} not found'.format(domainname))
    if len(dns_record) != 1:
        exit('Got {} records, expected exactly 1'.format(len(zone_info)))

    return {'id': dns_record[0]['id'], 'content': dns_record[0]['content'],
            'type': dns_record[0]['type'], 'zone_id': zone_id}


async def main():
    internet_ip = await getip()
    record_ip = checkip()
    if not internet_ip == record_ip['content']:
        print('IP has changed, updating {} to {} from {}'.format(hostname, internet_ip, record_ip['content']))
        new_record = {
            'name': hostname,
            'type': record_ip['type'],
            'content': internet_ip
        }
        try:
            update_record = cf.zones.dns_records.put(record_ip['zone_id'], record_ip['id'], data=new_record)
        except CloudFlare.exceptions.CloudFlareAPIError as e:
            exit('API call failed, error: {}'.format(e))
    else:
        print('IP for {} is already set to current IP {}'.format(hostname, internet_ip))

if __name__ == '__main__':
    loop.run_until_complete(main())
